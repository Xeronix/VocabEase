<h1>VocabEase</h1>

<h3>A program for memorizing words of the language you are interested in</h3>
<h1>
   <a href="https://gitlab.com/KirMozor/VocabEase/-/blob/main/VocabEase.csproj">  
      <img src="https://img.shields.io/badge/.NET%207.0-blueviolet?style=flat">  
   </a>
   <img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103?style=flat" >
   <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
   <a href="https://mastodon.social/@kirmozor">  
      <img src="https://img.shields.io/mastodon/follow/108763242400809137">  
   </a>
   <img src="https://img.shields.io/gitlab/stars/Xeronix/VocabEase.svg?style=flat">
</h1>

## Description⚡

VocabEase is a program designed to assist in the study of foreign languages through word memorization and test-taking. The program offers the following functionality:

* Upon initial launch, the user enters the system and specifies the language they already know (native language) and the language they want to learn (target foreign language). The user only needs to specify these languages once or can change them in the program settings.


* Dictionary loading: If the program already contains dictionaries for the specified languages, the user is prompted to choose one of them for study. If dictionaries are not available or are limited, the user is given the option to upload their own dictionary in the required format (e.g., SQLite or CSV). This allows the user to work with personal dictionaries or existing dictionaries within the program. The current dictionary can also be modified in the settings.


* Tests: After selecting a dictionary, a window appears with several words for memorization. Each word is accompanied by its translation.


* Word memorization: The user is required to memorize the translations of the presented words. When ready, they can proceed to testing.


* Test-taking: The user undergoes testing by translating the words into the target foreign language. If the user translates a word incorrectly, the program provides feedback and shows the correct answer. The user continues the test until they answer correctly.


* Protection against closure: The user cannot close the program window until they complete the test. If the user attempts to close the window, the computer shuts down, motivating the user to finish the test rather than seeking a way to exit.


* Auto-shutdown: If the user remains inactive for 15 minutes, the computer automatically shuts down, motivating the user to utilize their time effectively and discouraging them from minimizing the test window and engaging in other activities without dedicating time to language learning.


* Word repetition: Successfully memorized words can appear multiple times in other tests to reinforce learning. The user can adjust the number of repetitions.


* Translation support: The program supports word translation using Google Translate and Deepl, if the translation is not available in the dictionary database. This allows the user to obtain translations even for new words.

Thus, VocabEase provides an effective way to learn foreign languages by enabling users to memorize words and take tests to reinforce their knowledge.

## How to install✨

Instructions for installing VocabEase:

1. Install the .NET Core SDK on your computer if it's not already installed. You can download it from the official site: https://dotnet.microsoft.com/download.

2. Open a terminal or command line on your computer.

3. Download or clone the VocabEase repository, the commands below will be:
   * To download, you can click the button in the website or use the following command: `curl https://gitlab.com/Xeronix/VocabEase/-/archive/main/VocabEase-main.tar.gz`.
   * To clone the repository, use the following command: `git clone https://gitlab.com/Xeronix/VocabEase.git`.

4. If you have cloned a repository, skip this point. After downloading the archive you need to decompress it, you can use the usual programs with the interface or you can use the following command:
   * `tar xvf VocabEase-main.tar.gz`

5. After unpacking or cloning go to the folder, command below:
   * `cd VocabEase || cd VocabEase-main`

6. Next you need to compile the project from the source code:
   * `dotnet build --configuration Release`

Done, now the binary file to run is in the VocabEase/bin/Release folder named VocabEase

For correct work of the software add it to autostart on startup of your DE (for example in Gnome this is done with tweak tools)

## Community

My channel on Telegram: https://t.me/+-QPSC9v5pNthNTVi

There is also a mirror in the Mastodon: https://mastodon.social/@kirmozor

We also have a chat room, the link is in the attached message of the channel😁

## Sponsors
This project is sponsored by JetBrains and GitLab. Thanks to them, I have a license for the IDE and all the features of GitLab Ultimate🙂

<h2>  
   <a href="https://www.jetbrains.com/">  
      <img src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg" width="100" height="100">  
   </a>
   <a href="https://www.jetbrains.com/rider/">  
      <img src="https://resources.jetbrains.com/storage/products/company/brand/logos/Rider.svg" width="130" height="100">  
   </a>
   <a href="https://about.gitlab.com/why-gitlab/">  
      <img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg" width="200" height="100">  
   </a>
</h2>

## Helping the channel and development

If you want to help this project as well as my channels, the details are below:
* Ton - UQAt_Dl_TAOCJVe09oXMS1XeMHz2DXX2JrCdA0cSyJaCRd1Q
* Btc - 1CNBs7r8NpwvZXanw5fx7h6ggrBwGzZVkS
* Tether (TRC20) - TDskFXZUFG3UCvAmaCxkdtyudWzYs5t7yf
* Tether (BEP20) - 0x51d5a4530670d81a2382edf266f4d5975e83d377
* BNB (BEP20) - 0x51d5a4530670d81a2382edf266f4d5975e83d377
* Visa - 4400430240807740
