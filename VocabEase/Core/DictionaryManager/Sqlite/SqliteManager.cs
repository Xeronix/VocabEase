using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VocabEase.Core.Settings;

namespace VocabEase.Core.DictionaryManager.Sqlite;

/// <summary>
/// Class for dictionary management
/// </summary>
public class SqliteManager
{
    private HttpClient httpClient;
    private readonly SqliteConnection _connection;
    private readonly SqliteCommand _sqliteCommand;
    private SqliteTransaction _sqliteTransaction;

    /// <summary>
    /// Initialization of variables for working with the database and setting up a connection to the database
    /// </summary>
    /// <param name="nameOfDictionary">Database name (example: en_ru_5000words)</param>
    public SqliteManager(string nameOfDictionary)
    {
        Directory.CreateDirectory(AppSettings.PathDictionary);
        _connection = new SqliteConnection($"Data Source={AppSettings.PathDictionary}/{nameOfDictionary}.db;Cache=Shared;Mode=ReadWriteCreate;");
        _sqliteCommand = _connection.CreateCommand();
    }
    
    /// <summary>
    /// Creates a database for the dictionary with meta information about the target language in the dictionary, the native language for the user, a description of the dictionary and its author
    /// </summary>
    /// <param name="learningLanguage">target language in the dictionary</param>
    /// <param name="nativeLanguage">language the user already knows</param>
    /// <param name="description">dictionary description</param>
    /// <param name="author">author dictionary</param>
    public void CreateDictionaryDatabase(string learningLanguage, string nativeLanguage, string description, string author)
    {
        _connection.Open();

        _sqliteCommand.CommandText = "CREATE TABLE about_dictionary (learning_language TEXT, native_language TEXT, description TEXT, author TEXT);" +
                                     "CREATE TABLE dictionary (learning_word TEXT, transcription TEXT, native_word TEXT);" +
                                     "INSERT INTO about_dictionary (learning_language, native_language, description, author) VALUES (@LearningLanguage, @NativeLanguage, @Description, @Author);";
        _sqliteCommand.Parameters.AddWithValue("@LearningLanguage", learningLanguage);
        _sqliteCommand.Parameters.AddWithValue("@NativeLanguage", nativeLanguage);
        _sqliteCommand.Parameters.AddWithValue("@Description", description);
        _sqliteCommand.Parameters.AddWithValue("@Author", author);

        _sqliteCommand.ExecuteNonQuery();
        _connection.Close();
    }
    /// <summary>
    /// This method reads information about the dictionary
    /// </summary>
    public List<string> ReadAboutDictionary()
    {
        var aboutDictionary = new List<string>();
        
        _connection.Open();
        _sqliteCommand.CommandText =
            "SELECT learning_language, native_language, description, author FROM about_dictionary;";
        
        using (SqliteDataReader reader = _sqliteCommand.ExecuteReader())
        {
            reader.Read();
            aboutDictionary.Add(reader["learning_language"].ToString());
            aboutDictionary.Add(reader["native_language"].ToString());
            aboutDictionary.Add(reader["author"].ToString());
            aboutDictionary.Add(reader["description"].ToString());
        }

        _sqliteCommand.CommandText = "SELECT COUNT(*) AS row_count FROM dictionary;";
        using (SqliteDataReader reader = _sqliteCommand.ExecuteReader())
        {
            reader.Read();  
            aboutDictionary.Add(reader[0].ToString());
        }
        
        _connection.Close();
        return aboutDictionary;
    }
    /// <summary>
    /// This method inserts words into the database
    /// </summary>
    public void InsertDictionaryInDatabase(List<List<string>> listInWords)
    {
        _connection.Open();
        _sqliteTransaction = _connection.BeginTransaction();
        _sqliteCommand.Transaction = _sqliteTransaction;

        foreach (List<string> innerList in listInWords)
        {
            // Предполагается, что столбцы в таблице SQLite соответствуют элементам внутреннего списка
            _sqliteCommand.CommandText = "INSERT INTO dictionary (learning_word, transcription, native_word) VALUES (@learningWord, @transcription, @nativeWord)";
            _sqliteCommand.Parameters.Clear();
            
            _sqliteCommand.Parameters.AddWithValue("@learningWord", innerList[0]);
            switch (innerList.Count())
            {
                case 1:
                    _sqliteCommand.Parameters.AddWithValue("@transcription", DBNull.Value);
                    _sqliteCommand.Parameters.AddWithValue("@nativeWord", DBNull.Value);
                    break;
                case 2:
                    _sqliteCommand.Parameters.AddWithValue("@transcription", DBNull.Value);
                    _sqliteCommand.Parameters.AddWithValue("@nativeWord", innerList[1]);
                    break;
                case 3:
                    _sqliteCommand.Parameters.AddWithValue("@transcription", innerList[1]);
                    _sqliteCommand.Parameters.AddWithValue("@nativeWord", innerList[2]);
                    break;
            }

            _sqliteCommand.ExecuteNonQuery();
        }
        
        _sqliteTransaction.Commit();
        _connection.Close();
    }
    /// <summary>
    /// Gets a random word from the dictionary
    /// </summary>
    public List<string> GetRandomWord()
    {
        var randomWord = new List<string>();
        
        _connection.Open();
        _sqliteCommand.CommandText =
            "SELECT learning_word, transcription, native_word FROM dictionary ORDER BY RANDOM() LIMIT 1;";
        
        using (SqliteDataReader reader = _sqliteCommand.ExecuteReader())
        {
            reader.Read();
            randomWord.Add(reader["learning_word"].ToString());
            randomWord.Add(reader["transcription"].ToString());
            randomWord.Add(reader["native_word"].ToString());
        }
        
        _connection.Close();
        return randomWord;
    }
    /// <summary>
    /// This method will remove all duplicates if there are any in the dictionary
    /// </summary>
    public void DeleteDuplicates()
    {
        _connection.Open();

        _sqliteCommand.CommandText = "CREATE TABLE temp_table AS SELECT DISTINCT * FROM dictionary;" +
                                     "DELETE FROM dictionary; " +
                                     "INSERT INTO dictionary SELECT * FROM temp_table";

        _sqliteCommand.ExecuteNonQuery();
        _connection.Close();
    }
}