using VocabEase.Core.DictionaryManager.Sqlite;
using VocabEase.Core.Settings;

namespace VocabEase.Core.DictionaryManager;

/// <summary>
/// Class for parsing a list of dictionaries
/// </summary>
public class ParseDictionary
{
    /// <summary>
    /// Returns a list of dictionaries located in the program directory
    /// </summary>
    public static string[] ParseInDirectoryDictionary()
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(AppSettings.PathDictionary);
        FileInfo[] files = directoryInfo.GetFiles();

        string[] fileNamesWithoutDb = new string[files.Length];

        for (int i = 0; i < files.Length; i++)
        {
            string fileName = files[i].Name;
            string fileNameWithoutDb = fileName.Replace(".db", "");
            fileNamesWithoutDb[i] = fileNameWithoutDb;
        }
        
        return fileNamesWithoutDb;
    }
}