namespace VocabEase.Core.DictionaryManager;

/// <summary>
/// Class for parsing dictionaries from txt, csv (in the future)
/// </summary>
public class ParseDictionaryInFile
{
    /// <summary>
    /// This method parses a txt file in the following format: "learned word";"native word", "learned word";"transcription";"native word" or "learned word"
    /// </summary>
    /// <param name="filePath">Path to the dictionary file</param>
    public static List<List<string>> ParseTxtFile(string filePath)
    {
        var listInWords = new List<List<string>>();
        foreach (string line in File.ReadLines(filePath))
        {
            var tempList = new List<string>{};
            string[] parts = line.Split(new[] { "\";\"" }, StringSplitOptions.None);
            
            tempList.Add(parts[0].Replace("\"", ""));
            switch (parts.Length)
            {
                case 2:
                    tempList.Add(parts[1].Replace("\"", ""));
                    break;
                case 3:
                    tempList.Add(parts[1].Replace("\"", ""));
                    tempList.Add(parts[2].Replace("\"", "") );
                    break;
            }
            listInWords.Add(tempList);
        }

        return listInWords;
    }
}