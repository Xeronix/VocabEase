using System.Globalization;
using VocabEase.Core.Settings;

namespace VocabEase.Core.Localization.Helper;

/// <summary>
/// This class implements methods for localization
/// </summary>
public class LocalizationManager
{
    /// <summary>
    /// List of languages supported in the program
    /// </summary>
    public static string[] listLanguages = new[]
    {
        "ru", "en"
    };

    /// <summary>
    /// Method for setting the language in the program
    /// </summary>
    /// <param name="language"></param>
    public static void SetLanguage(string language = null)
    {
        switch (language)
        {
            case "ru":
                AppSettings.InstalledLanguage = "ru";
                break;
            case "en":
                AppSettings.InstalledLanguage = "en";
                break;
        }
        CultureInfo.CurrentUICulture = CultureInfo.GetCultureInfo(AppSettings.InstalledLanguage);
        SettingsReaderWritter.WriteConfig();
    }
}