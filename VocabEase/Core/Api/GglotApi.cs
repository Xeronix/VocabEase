using GglotAiApi;

namespace VocabEase.Core.Api;

public class GglotApi
{
    public static string GetTranscription(string paragraph)
    {
        Transcription transcription = new Transcription();
        List<TranscriptItem> transcripts = transcription.GetTranscript(paragraph).Result;

        string result = String.Empty;
        foreach (var transcript in transcripts)
        {
            foreach (var ipa in transcript.Ipa)
            {
                result += $"[{ipa}] ";
            }
        }
        
        return result;
    }
}