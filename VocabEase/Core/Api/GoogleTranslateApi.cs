using GTranslatorAPI;
using VocabEase.Core.Settings;

namespace VocabEase.Core.Api;

/// <summary>
/// A class for interacting with the Google Translate API
/// </summary>
public class GoogleTranslateApi
{
    private readonly GTranslatorAPISettings _translatorSettings = new GTranslatorAPISettings();
    private readonly GTranslatorAPIClient _translatorClient;

    /// <summary>
    /// Initializing settings for the translator
    /// </summary>
    public GoogleTranslateApi()
    {
        _translatorSettings.NetworkQueryTimeout = AppSettings.NetworkQueryTimeout;
        _translatorClient = new GTranslatorAPIClient(_translatorSettings);
    }

    /// <summary>
    /// The method for translating a word
    /// </summary>
    /// <param name="text">Text to translate</param>
    /// <param name="targetLanguage">The language from which you are translating (e.g. en)</param>
    /// <param name="outputLanguage">The language into which you are translating (e.g. ru)</param>
    /// <returns>The translated text is returned</returns>
    public async Task<string> WordTranslation(string text, string targetLanguage, string outputLanguage)
    {
        var result = await _translatorClient.TranslateAsync(targetLanguage, outputLanguage, text);
        return result.TranslatedText;
    }
}   
