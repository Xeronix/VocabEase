using DeepL;
using VocabEase.Core.Settings;

namespace VocabEase.Core.Api;

/// <summary>
/// A class for interacting with the Deepl Translate API
/// </summary>
public class DeeplApi
{
    private readonly TranslatorOptions _translatorSettings = new TranslatorOptions();
    private readonly Translator _translatorClient;

    /// <summary>
    /// Initializing settings for the translator
    /// </summary>
    /// <exception cref="NullReferenceException">If there is no token there will be an error</exception>
    public DeeplApi()
    {
        if (AppSettings.TokenDeeplTranslate != null)
        {
            _translatorSettings.MaximumNetworkRetries = AppSettings.MaximumNetworkRetries;
            _translatorSettings.PerRetryConnectionTimeout =
                TimeSpan.FromMilliseconds(AppSettings.NetworkQueryTimeout);

            _translatorClient = new Translator(AppSettings.TokenDeeplTranslate, _translatorSettings);
        }
        else
        {
            throw new NullReferenceException("Token Deepl is null");
        }
    }

    /// <summary>
    /// The method for translating a word
    /// </summary>
    /// <param name="text">Text to translate</param>
    /// <param name="targetLanguage">The language from which you are translating (e.g. en)</param>
    /// <param name="outputLanguage">The language into which you are translating (e.g. ru)</param>
    /// <returns>The translated text is returned</returns>
    public async Task<string> WordTranslation(string text, string targetLanguage, string outputLanguage)
    {
        var translationResult = await _translatorClient.TranslateTextAsync(text, targetLanguage, outputLanguage);
        return translationResult.Text;
    }
}
