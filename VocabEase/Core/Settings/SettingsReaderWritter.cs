using System.Text;
using Tomlyn;

namespace VocabEase.Core.Settings;

/// <summary>
/// This class is needed to write and read the config file
/// </summary>
public class SettingsReaderWritter
{
    /// <summary>
    /// This method is used to write the configuration file from the AppSettings configuration class
    /// </summary>
    public static void WriteConfig()
    {
        Dictionary<string, string> config = new Dictionary<string, string>();
        
        // The solution is not the best, but I will fix it later
        config["NetworkQueryTimeout"] = Convert.ToString(AppSettings.NetworkQueryTimeout);
        config["MaximumNetworkRetries"] = Convert.ToString(AppSettings.MaximumNetworkRetries);
        config["TokenDeeplTranslate"] = Convert.ToString(AppSettings.TokenDeeplTranslate);
        config["InstalledLanguage"] = Convert.ToString(AppSettings.InstalledLanguage);
        config["TranslationServices"] = Convert.ToString(AppSettings.TranslationServices);
        config["SelectLearningDictionaries"] = Convert.ToString(AppSettings.SelectLearningDictionaries);
        config["RepeatTestWords"] = Convert.ToString(AppSettings.RepeatTestWords);
        config["MinuteTimer"] = Convert.ToString(AppSettings.MinuteTimer);

        string configString = string.Join(Environment.NewLine, config.Select(kv => $"{kv.Key} = \"{kv.Value}\""));
        Directory.CreateDirectory(Path.GetDirectoryName(AppSettings.PathSettings));

        File.WriteAllText(AppSettings.PathSettings, configString, Encoding.UTF8);
    }
    
    /// <summary>
    /// This class reads the configuration from the TOML file and stores all the information in AppSettings variables for later use in code
    /// </summary>
    /// <exception cref="FileNotFoundException">This error occurs if the file path is specified incorrectly</exception>
    public static void ReadConfig()
    {
        if (File.Exists(AppSettings.PathSettings))
        {
            string configText = File.ReadAllText(AppSettings.PathSettings);
            var configModel = Toml.ToModel(configText);
            
            // The solution is not the best, but I will fix it later
            AppSettings.NetworkQueryTimeout = Convert.ToInt32(configModel["NetworkQueryTimeout"]);
            AppSettings.MaximumNetworkRetries = Convert.ToInt32(configModel["MaximumNetworkRetries"]);
            AppSettings.TokenDeeplTranslate = Convert.ToString(configModel["TokenDeeplTranslate"]);
            AppSettings.InstalledLanguage = Convert.ToString(configModel["InstalledLanguage"]);
            AppSettings.TranslationServices = Convert.ToString(configModel["TranslationServices"]);
            AppSettings.SelectLearningDictionaries = Convert.ToString(configModel["SelectLearningDictionaries"]);
            AppSettings.RepeatTestWords = Convert.ToByte(configModel["RepeatTestWords"]);
            AppSettings.MinuteTimer = Convert.ToByte(configModel["MinuteTimer"]);
        }
        else
        {
            throw new FileNotFoundException($"File in the \"{AppSettings.PathSettings}\" path is not found");
        }
    }
}
