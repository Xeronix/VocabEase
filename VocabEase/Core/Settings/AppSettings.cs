namespace VocabEase.Core.Settings;

/// <summary>
/// A class with program settings
/// </summary>
public static class AppSettings
{
    /// <summary>
    /// Timeout for receiving a request from translation services (default 10000 ms (10 second) )
    /// </summary>
    public static int NetworkQueryTimeout { get; set; } = 10000;

    /// <summary>
    /// Maximum number of retries for translation
    /// </summary>
    public static int MaximumNetworkRetries { get; set; } = 5;
    
    /// <summary>
    /// Path to VocabEase settings file
    /// </summary>
    public static string PathSettings { get; } = @$"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/.local/share/vocabease/settings.toml";
    
    /// <summary>
    /// Path to databases with dictionaries
    /// </summary>
    public static string PathDictionary { get; } = @$"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/.local/share/vocabease/dictionary";

    /// <summary>
    /// Token for Deepl translator, by default null since not everyone has a token from it
    /// </summary>
    public static string TokenDeeplTranslate { get; set; } = null;

    /// <summary>
    /// Language selection different from the default system language
    /// </summary>
    public static string InstalledLanguage { get; set; } = null;

    /// <summary>
    /// Translation service used in the program
    /// </summary>
    public static string TranslationServices { get; set; } = "Google";

    /// <summary>
    /// Vocabulary that the user has chosen to study
    /// </summary>
    public static string SelectLearningDictionaries { get; set; } = null;

    /// <summary>
    /// 
    /// </summary>
    public static byte RepeatTestWords = 3;

    /// <summary>
    /// 
    /// </summary>
    public static byte MinuteTimer = 15;
}