using Spectre.Console;
using VocabEase.Core.Api;
using VocabEase.Core.DictionaryManager.Sqlite;
using VocabEase.Core.Settings;

namespace VocabEase.Tui.Windows.OtherMenu;

public class StartLearningMenu : BaseWindow
{
    private static List<string> randomWord = GetRandomWordAndCheck();
    public static void CreateBaseForms()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            if (AppSettings.SelectLearningDictionaries != "")
            {
                MainTable.AddColumn(new TableColumn("Timer: null")
                        .RightAligned()
                    )
                    .Border(TableBorder.None);
                AnsiConsole.Write(MainTable);
                
                AnsiConsole.Write(new Markup("[bold][red]Не выключайте программу! Компьютер будет выключен, заверщите тест до окончания таймера иначе компьютер также будет выключен[/][/]"));
                TestWordMemorization();
            }
            else
            {
                AnsiConsole.Write(new Markup("[bold][red]Вы не выбрали словарь для изучения. Нажмите Enter чтобы перейти в менеджер словарей дабы выбрать словарь[/][/]"));
                AnsiConsole.Console.Input.ReadKey(true);
                ManagementDictionaries.CreateBaseForms();
            }
        });
    }

    private static void TestWordMemorization()
    {
        AnsiConsole.Write($"\n\nПостарайтесь запомнить:\n\nИзучаемое слово (предложение): {randomWord[0]}\nТранскрипция: {randomWord[1]}\nПеревод: {randomWord[2]}");
        AnsiConsole.Write(new Markup("\n\n[bold]Как запомните, нажмите Enter чтобы продолжить[/]"));
        
        AnsiConsole.Console.Input.ReadKey(true);
        TestLearningWords();
    }

    private static void TestLearningWords()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var testArray = new string[]
            {
                GetRandomWordAndCheck()[2],
                GetRandomWordAndCheck()[2],
                GetRandomWordAndCheck()[2],
                GetRandomWordAndCheck()[2],
                randomWord[2]
            };
        
            // 
            Random random = new Random();
            for (int i = testArray.Length - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                string temp = testArray[i];
                testArray[i] = testArray[j];
                testArray[j] = temp;
            }

            var test = AnsiConsole.Prompt(new SelectionPrompt<string>()
                .Title("Вот вам тест из 5 слов, пожалуйста укажите правильное слово")
                .AddChoices(testArray));

            if (test == randomWord[2])
            {
                AnsiConsole.Write(new Markup("[bold]Отлично! Вы справились :)[/]"));
                AnsiConsole.Console.Input.ReadKey(true);
            }
            else
            {
                AnsiConsole.Write(new Markup("[bold][red]Вы выбрали не правильное слово, нажмите Enter чтобы повторить его[/][/]"));
                AnsiConsole.Console.Input.ReadKey(true);
                TestWordMemorization();
            }
        });
    }

    private static List<string> GetRandomWordAndCheck()
    {
        var sqliteManager = new SqliteManager(AppSettings.SelectLearningDictionaries);
        var rawRandomWord = sqliteManager.GetRandomWord();
        var aboutDictionary = sqliteManager.ReadAboutDictionary();

        if (rawRandomWord[1] == "")
        {
            AnsiConsole.Status()
                .Start("Получаем транскрипцию слова...", ctx =>
                {
                    ctx.Spinner(Spinner.Known.Ascii);
                    rawRandomWord[1] = GglotApi.GetTranscription(rawRandomWord[0]);
                });   
        }
        if (rawRandomWord[2] == "")
        {
            AnsiConsole.Status()
                .Start("Получаем перевод слова...", ctx =>
                {
                    ctx.Spinner(Spinner.Known.Ascii);
                    if (AppSettings.TranslationServices == "Google")
                    {
                        var googleTranslateApi = new GoogleTranslateApi();
                        rawRandomWord[2] = googleTranslateApi.WordTranslation(rawRandomWord[0], aboutDictionary[0], aboutDictionary[1]).Result;
                    }
                    else
                    {
                        var deeplTranslateApi = new DeeplApi();
                        rawRandomWord[2] = deeplTranslateApi.WordTranslation(rawRandomWord[0], aboutDictionary[0], aboutDictionary[1]).Result;
                    }   
                });
        }

        return rawRandomWord;
    }
}