using Spectre.Console;


namespace VocabEase.Tui.Windows.OtherMenu;

/// <summary>
/// This class creates a window for FAQ (program help) and opens it for reading
/// </summary>
public class FaqMenu : BaseWindow
{
    /// <summary>
    /// Creates a basic window for selecting the desired program assistance
    /// </summary>
    public static void CreateBaseForms()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var selectFaqMenu = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title(Core.Localization.OtherMenu.FaqMenu.FaqMenuTitle)
                    .AddChoices(new string[]
                    {
                        Core.Localization.OtherMenu.FaqMenu.SolvingInternetProblems,
                        Core.Localization.OtherMenu.FaqMenu.TranslationServices,
                        Core.Localization.OtherMenu.FaqMenu.ProgramLocalizations,
                        Core.Localization.OtherMenu.FaqMenu.ProjectDocumentation,
                        //Core.Localization.OtherMenu.FaqMenu.LoadingDictionaries,
                        Core.Localization.OtherMenu.FaqMenu.BackInMenu
                    })
            );

            if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.SolvingInternetProblems)
                CreateHelpForms(Core.Localization.OtherMenu.FaqMenu.SolvingInternetProblemsText);
            if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.TranslationServices)
                CreateHelpForms(Core.Localization.OtherMenu.FaqMenu.TranslationServicesText);
            if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.ProgramLocalizations)
                CreateHelpForms(Core.Localization.OtherMenu.FaqMenu.ProgramLocalizationText);
            if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.ProjectDocumentation)
                CreateHelpForms(Core.Localization.OtherMenu.FaqMenu.ProjectDocumentationText);
            //if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.LoadingDictionaries)
            //    CreateHelpForms("");
            if (selectFaqMenu == Core.Localization.OtherMenu.FaqMenu.BackInMenu)
                MainMenu.CreateBaseForms();
        });
    }

    /// <summary>
    /// Method for outputting text with, and returns back to the menu
    /// </summary>
    /// <param name="text">Text with help from Core.Localizations.OtherMenu.FaqMenu</param>
    private static void CreateHelpForms(string text)
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            AnsiConsole.Write(new Markup(text));
            AnsiConsole.Console.Input.ReadKey(true);
            CreateBaseForms();
        });
    }
}