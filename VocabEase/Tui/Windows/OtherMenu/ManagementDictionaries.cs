using Spectre.Console;
using VocabEase.Core.DictionaryManager;
using VocabEase.Core.DictionaryManager.Sqlite;
using VocabEase.Core.Settings;

namespace VocabEase.Tui.Windows.OtherMenu;

/// <summary>
/// This class creates a window for managing dictionaries, helps to select a dictionary from existing ones or create a new one for study
/// </summary>
public class ManagementDictionaries : BaseWindow
{
    /// <summary>
    /// Create a basic selection window, select an existing dictionary, create a new one, or exit to the main menu
    /// </summary>
    public static void CreateBaseForms()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var selectMenu = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title(Core.Localization.OtherMenu.ManagementDictionaries.MenuTitle)
                    .AddChoices(new string[]
                    {
                        Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionary,
                        Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionary,
                        Core.Localization.OtherMenu.ManagementDictionaries.BackInMenu,
                    })
            );
            
            if (selectMenu == Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionary)
                SelectExistingDictionary();
            if (selectMenu == Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionary)
                CreateNewDictionary();
            if (selectMenu == Core.Localization.OtherMenu.ManagementDictionaries.BackInMenu)
                MainMenu.CreateBaseForms();
                
            AnsiConsole.Console.Input.ReadKey(true);
        });
    }
    /// <summary>
    /// Dictionary selection window
    /// </summary>
    private static void SelectExistingDictionary()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);

            var listDictionaries = ParseDictionary.ParseInDirectoryDictionary();
            Array.Resize(ref listDictionaries, listDictionaries.Length + 1);
            listDictionaries[listDictionaries.Length - 1] = Core.Localization.OtherMenu.ManagementDictionaries.BackInMenu;

            var selectDictionaries = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title(Core.Localization.OtherMenu.ManagementDictionaries.SelectExistinsDictionaryTitle)
                    .AddChoices(listDictionaries));
            
            if (selectDictionaries == Core.Localization.OtherMenu.ManagementDictionaries.BackInMenu)
                CreateBaseForms();
            else
            {
                ReadAboutDictionary(selectDictionaries);
            }
        });
    }
    /// <summary>
    /// This method displays information about the dictionary and lets the user choose whether to use it or not.
    /// </summary>
    /// <param name="dictionary">dictionary name</param>
    private static void ReadAboutDictionary(string dictionary)
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);

            var sqliteManager = new SqliteManager(dictionary);
            var aboutInformDictionary = sqliteManager.ReadAboutDictionary();
            
            AnsiConsole.Markup($"[bold]{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryAboutText}[/] {dictionary}");
            AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryLearnedLanguage} {aboutInformDictionary[0]}");
            AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryNativeLanguage} {aboutInformDictionary[1]}");
            AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryCountWords} {aboutInformDictionary[4]}");
            AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryAuthor} {aboutInformDictionary[2]}");
            AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionaryDescription} \n{aboutInformDictionary[3]}\n");

            if (AnsiConsole.Confirm(Core.Localization.OtherMenu.ManagementDictionaries.SelectExistingDictionarySelectDictionary))
            {
                AppSettings.SelectLearningDictionaries = dictionary;
                SettingsReaderWritter.WriteConfig();
                
                AnsiConsole.WriteLine(Core.Localization.OtherMenu.ManagementDictionaries.SelectDictionariesAllRight);
                AnsiConsole.Console.Input.ReadKey(true);
                MainMenu.CreateBaseForms();
            }
            else
            {
                AnsiConsole.WriteLine(Core.Localization.OtherMenu.ManagementDictionaries.SelectDictionariesPickAnotherOne);
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
        });
    }
    /// <summary>
    /// This method creates a new custom dictionary and imports words from a txt file
    /// </summary>
    private static void CreateNewDictionary()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            AnsiConsole.Markup(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryText);
            var nameDictionary = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryNameDictionaryAsk);
            var learningLanguage = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryLearningLanguageAsk);
            var nativeLanguage = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryNativeLanguageAsk);
            var description = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryDescriptionAsk);
            var author = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryAuthorAsk);
            
            AnsiConsole.Markup(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummary +
                                  $"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryNameDictionary}{nameDictionary}" +
                                  $"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryLearningLanguage}{learningLanguage}" +
                                  $"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryNativeLanguage}{nativeLanguage}" +
                                  $"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryAuthor}{author}" +
                                  $"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryDescription}{description}\n");
            if (AnsiConsole.Confirm(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySummaryConfirmation))
            {
                SqliteManager sqliteManager = new SqliteManager(nameDictionary);
                sqliteManager.CreateDictionaryDatabase(learningLanguage, nativeLanguage, description, author);
                
                AnsiConsole.WriteLine($"{Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySuccesCreate}{AppSettings.PathDictionary}");
                
                string pathToFile;
                while (true)
                {
                    pathToFile = AnsiConsole.Ask<string>(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryInsertPathToFile);

                    if (File.Exists(pathToFile))
                    {
                        break;
                    }
                    else
                    {
                        if (pathToFile == "break")
                        {
                            File.Delete($"{AppSettings.PathDictionary}/{nameDictionary}.db");
                            CreateBaseForms();
                        }
                        else
                        {
                            AnsiConsole.WriteLine(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryFileNotFound);
                        }
                    }
                }

                AnsiConsole.Status()
                    .Start(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionaryStatus, ctx =>
                    {
                        ctx.Spinner(Spinner.Known.Ascii);
                        var listWords = ParseDictionaryInFile.ParseTxtFile(pathToFile);
                        sqliteManager.InsertDictionaryInDatabase(listWords);
                    });
                AnsiConsole.WriteLine(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictionarySuccess);
                
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
            else
            {
                AnsiConsole.WriteLine(Core.Localization.OtherMenu.ManagementDictionaries.CreateNewDictiionaryAbort);
                File.Delete($"{AppSettings.PathDictionary}/{nameDictionary}.db");
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
        });
    }
}