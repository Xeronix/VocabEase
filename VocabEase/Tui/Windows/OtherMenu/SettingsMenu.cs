using Spectre.Console;
using VocabEase.Core.Api;
using VocabEase.Core.Localization.Helper;
using VocabEase.Core.Settings;

namespace VocabEase.Tui.Windows.OtherMenu;

/// <summary>
/// Settings program menu
/// </summary>
public class SettingsMenu : BaseWindow
{
    /// <summary>
    /// This method creates a basic menu window and controls transitions to the settings of other parameters, and returns back to the main menu
    /// </summary>
    public static void CreateBaseForms()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var selectMenu = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title(Core.Localization.OtherMenu.SettingsMenu.SettingsTitle)
                    .AddChoices(new string[]
                    {
                        Core.Localization.OtherMenu.SettingsMenu.ChangeLanguage,
                        Core.Localization.OtherMenu.SettingsMenu.InsertDeeplToken,
                        Core.Localization.OtherMenu.SettingsMenu.MaximumNetworkRetries,
                        Core.Localization.OtherMenu.SettingsMenu.NetworkQueryTimeout,
                        Core.Localization.OtherMenu.SettingsMenu.BackInMenu
                    })
            );

            if (selectMenu == Core.Localization.OtherMenu.SettingsMenu.ChangeLanguage)
                ChangeLanguageMenu();
            if (selectMenu == Core.Localization.OtherMenu.SettingsMenu.InsertDeeplToken)
                InsertDeeplTokenMenu();
            if (selectMenu == Core.Localization.OtherMenu.SettingsMenu.MaximumNetworkRetries)
                MaximumNetworkRetries();
            if (selectMenu == Core.Localization.OtherMenu.SettingsMenu.NetworkQueryTimeout)
                NetworkQueryTimeout();
            if (selectMenu == Core.Localization.OtherMenu.SettingsMenu.BackInMenu)
                MainMenu.CreateBaseForms();
        });
    }

    /// <summary>
    /// This method sets NetworkQueryTimeout from the user, or gives an error if the user entered a non-number
    /// </summary>
    private static void NetworkQueryTimeout()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var networkQueryTimeout = AnsiConsole.Prompt(
                new TextPrompt<string>(Core.Localization.OtherMenu.SettingsMenu.NetworkQueryTimeoutHelp)).Trim();

            try
            {
                AppSettings.NetworkQueryTimeout = Convert.ToInt32(networkQueryTimeout);
                SettingsReaderWritter.WriteConfig();
                
                AnsiConsole.Write(Core.Localization.OtherMenu.SettingsMenu.NetworkQueryTimeoutSuccess);
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
            catch (FormatException)
            {
                AnsiConsole.Write(Core.Localization.OtherMenu.SettingsMenu.NetworkQueryTimeoutError);
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
        });
    }

    /// <summary>
    /// This method sets MaximumNetworkRetries from the user, or gives an error if the user entered a non-number
    /// </summary>
    private static void MaximumNetworkRetries()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var maximumNetworkRetries = AnsiConsole.Prompt(
                new TextPrompt<string>(Core.Localization.OtherMenu.SettingsMenu.MaximumNetworkRetriesHelp)).Trim();

            try
            {
                AppSettings.MaximumNetworkRetries = Convert.ToInt32(maximumNetworkRetries);
                SettingsReaderWritter.WriteConfig();
                
                AnsiConsole.Write(Core.Localization.OtherMenu.SettingsMenu.MaximumNetworkRetriesSuccess);
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
            catch (FormatException)
            {
                AnsiConsole.Write(Core.Localization.OtherMenu.SettingsMenu.MaximumNetworkRetriesError);
                AnsiConsole.Console.Input.ReadKey(true);
                CreateBaseForms();
            }
        });
    }

    /// <summary>
    /// This method applies the language selected by the user then writes it down
    /// </summary>
    private static void ChangeLanguageMenu()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var selectLanguage = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .PageSize(10)
                    .Title(Core.Localization.FirstLaunch.LanguageSelectionMenu)
                    .AddChoices(LocalizationManager.listLanguages));

            LocalizationManager.SetLanguage(selectLanguage);
            CreateBaseForms();
        });
    }
    
    /// <summary>
    /// This method receives the Deepl token and verifies it, then writes it or not
    /// </summary>
    private static void InsertDeeplTokenMenu()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);

            var deeplToken = AnsiConsole.Prompt(
                new TextPrompt<string>(Core.Localization.OtherMenu.SettingsMenu.InsertDeeplTokenMenu)).Trim(); // Please enter a token from Deepl
            
            switch (deeplToken)
            {
                case "n": // If a user makes a mistake, they enter "n" to go back to the settings
                    CreateBaseForms();
                    break;
                default: 
                    AppSettings.TokenDeeplTranslate = deeplToken;
                    var deeplTranslate = new DeeplApi();

                    try
                    {
                        AnsiConsole.Status() // Graphical display of token verification
                            .Start(Core.Localization.OtherMenu.SettingsMenu.CheckingDeeplToken, ctx =>
                            {
                                ctx.Spinner(Spinner.Known.Ascii);
                                var check = deeplTranslate.WordTranslation("check", "en", "ru").Result; // Translation test with a token from the user
                                AppSettings.TranslationServices = "Deepl";
                            });
                        
                        AnsiConsole.WriteLine(Core.Localization.OtherMenu.SettingsMenu.DeeplTokenMatched);
                        SettingsReaderWritter.WriteConfig(); // Writing the token to the settings file
                        AnsiConsole.Console.Input.ReadKey(true);
                        CreateBaseForms();
                    }
                    catch (AggregateException) // If the token is wrong
                    {
                        AppSettings.TokenDeeplTranslate = null;
                        AnsiConsole.WriteLine(Core.Localization.OtherMenu.SettingsMenu.DeeplTokenDidntMatch);
                        
                        AnsiConsole.Console.Input.ReadKey(true);
                        CreateBaseForms();
                    }
                    
                    break;
            }
        });
    }
}