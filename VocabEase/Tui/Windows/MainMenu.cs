using Spectre.Console;
using VocabEase.Tui.Windows.OtherMenu;

namespace VocabEase.Tui.Windows;

/// <summary>
/// Main program window, menu
/// </summary>
public class MainMenu : BaseWindow
{
    /// <summary>
    /// Create a basic window with a menu that switches to other menus for setup, dictionary loading, word study, and program help
    /// </summary>
    public static void CreateBaseForms()
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            AnsiConsole.Write(VocabEaseText);
            
            var selectMenu = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title(Core.Localization.MainMenu.Title)
                    .AddChoices(new string[]
                    {
                        Core.Localization.MainMenu.StartLearning, 
                        Core.Localization.MainMenu.LoadWords,
                        Core.Localization.MainMenu.Settings,
                        Core.Localization.MainMenu.FAQ,
                        Core.Localization.MainMenu.Exit
                    })
                );

            if (selectMenu == Core.Localization.MainMenu.StartLearning)
                StartLearningMenu.CreateBaseForms();
            if (selectMenu == Core.Localization.MainMenu.LoadWords)
                ManagementDictionaries.CreateBaseForms();
            if (selectMenu == Core.Localization.MainMenu.Settings)
                SettingsMenu.CreateBaseForms();
            if (selectMenu == Core.Localization.MainMenu.FAQ)
                FaqMenu.CreateBaseForms();
            if (selectMenu == Core.Localization.MainMenu.Exit)
            {
                ClearWindow();
                Environment.Exit(1);   
            }
        });
    }
}