using Spectre.Console;
using VocabEase.Core.Localization.Helper;
using VocabEase.Core.Settings;

namespace VocabEase.Tui.Windows;

/// <summary>
/// Class for the first start of the program
/// </summary>
public class FirstLaunch : BaseWindow
{
    /// <summary>
    /// Creating a program window, initial setup and switching to the main window
    /// </summary>
    public static void CreateBaseForms(bool restart = false)
    {
        ClearWindow();
        AnsiConsole.AlternateScreen(() =>
        {
            MainTable.AddColumn(new TableColumn(Core.Localization.FirstLaunch.WelcomeTextColumn));
            MainTable.AddRow(Core.Localization.FirstLaunch.WelcomeText);
            MainTable.AddRow(Core.Localization.FirstLaunch.TextForFirstSetup);

            AnsiConsole.Write(VocabEaseText);
            AnsiConsole.Write(MainTable);

            if (!restart) // Restart the window when the user has selected a language, so that the user can read the introduction to the program in the desired language. If the restart was done, the language will not be asked again
            {
                var selectLanguage = AnsiConsole.Prompt(
                    new SelectionPrompt<string>()
                        .PageSize(10)
                        .Title(Core.Localization.FirstLaunch.LanguageSelectionMenu)
                        .AddChoices(LocalizationManager.listLanguages));
                
                LocalizationManager.SetLanguage(selectLanguage);
                CreateBaseForms(true);
            }

            if (AnsiConsole.Confirm(Core.Localization.FirstLaunch.AskDeeplToken))
            {
                AppSettings.TokenDeeplTranslate = AnsiConsole.Ask<string>(Core.Localization.FirstLaunch.DeeplToken);
                AppSettings.TranslationServices = "Deepl";
            }

            SettingsReaderWritter.WriteConfig();
            MainMenu.CreateBaseForms();
        });
    }
}