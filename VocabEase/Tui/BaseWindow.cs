using Spectre.Console;

namespace VocabEase.Tui;

/// <summary>
/// Base class for windows where widgets used by the whole program are stored
/// </summary>
public class BaseWindow
{
    protected static Table MainTable = new Table()
        .Centered()
        .Expand();
    
    protected static readonly Rule VocabEaseText = new Rule(Core.Localization.BaseWindow.VocabEaseText);

    /// <summary>
    /// Method for cleaning widgets for later use
    /// </summary>
    protected static void ClearWindow()
    {
        MainTable = new Table()
            .Centered()
            .Expand();
        AnsiConsole.Clear();
    }
}