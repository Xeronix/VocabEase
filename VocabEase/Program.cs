﻿using Spectre.Console;
using VocabEase.Core.Localization.Helper;
using VocabEase.Core.Settings;
using VocabEase.Tui.Windows;

namespace VocabEase
{
    class Program
    {
        static void Main()
        {
            if (!AnsiConsole.Profile.Capabilities.AlternateBuffer) // If your terminal does not support AlternateBuffer (separate window)
            {
                AnsiConsole.MarkupLine(Core.Localization.Program.AlternateScreenIsNotSupported);
                return;
            }

            if (!File.Exists(AppSettings.PathSettings)) // If the program is launched for the first time (and therefore without a config) run FirstLaunch
                FirstLaunch.CreateBaseForms();
            else // If not the first time, read the config and start the main window of the program - menu
            {
                SettingsReaderWritter.ReadConfig();
                LocalizationManager.SetLanguage();
                MainMenu.CreateBaseForms();
            }
        }
    }
}